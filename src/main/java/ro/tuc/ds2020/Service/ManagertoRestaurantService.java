package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.ManagertoRestaurant;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Models.Restaurant;
import ro.tuc.ds2020.Repository.ManagertoRestaurantRepo;

import java.util.List;

@Service
public class ManagertoRestaurantService {
    private final ManagertoRestaurantRepo repo;
    @Autowired
    public ManagertoRestaurantService(ManagertoRestaurantRepo repo) {
        this.repo = repo;
    }
    public ManagertoRestaurant save(ManagertoRestaurant x){
        return repo.save(x);
    }

    public Restaurant findRestbyidManager(int id){
        for(ManagertoRestaurant i:repo.findAll()){
            if(i.getAccount().getAccount_id()==id){
                return i.getRestaurant();
            }
        }
        return null;
    }
}
