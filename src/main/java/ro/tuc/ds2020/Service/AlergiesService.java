package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.DpToAlergies;
import ro.tuc.ds2020.Models.DptoWishlist;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Repository.DptoAlergiesRepo;

import java.util.ArrayList;
import java.util.List;
@Service
public class AlergiesService{
    private final DptoAlergiesRepo dptoAlergiesRepo;

    @Autowired
    public AlergiesService(DptoAlergiesRepo dptoAlergiesRepo) {
        this.dptoAlergiesRepo = dptoAlergiesRepo;
    }

    public DpToAlergies addAlergies(DpToAlergies p) {
        return dptoAlergiesRepo.save(p);
    }

    public List<Product> getAlergiesProductByaccId(int id){
        List<Product> productList=new ArrayList<>();
        for(DpToAlergies i:dptoAlergiesRepo.findAll())
            if(i.getAccount().getAccount_id()==id)
                productList.add(i.getProduct());
        return productList;
    }
}


