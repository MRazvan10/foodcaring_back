package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.*;
import ro.tuc.ds2020.Repository.MenutoRestaurantRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenutoRestaurantService {
    private final MenutoRestaurantRepo repo;

    public MenutoRestaurantService(MenutoRestaurantRepo repo) {
        this.repo = repo;
    }
    public MenuToRestaurant save(MenuToRestaurant x){
        return repo.save(x);
    }

    public void delete(MenuToRestaurant x){
        for(MenuToRestaurant i:repo.findAll()){
            if(i.getMenu().getMenu_name().equals(x.getMenu().getMenu_name()) &&
                i.getX().getRestaurant_name().equals(x.getX().getRestaurant_name())){
                repo.delete(i);
                break;
            }
        }
    }

    public void delete(int menuid){
        for(MenuToRestaurant x:repo.findAll())
            if(x.getMenu().getMenu_id()==menuid){
                repo.delete(x);
            }

    }

    public List<Menu> getMenuByRestaurant(Restaurant restaurant){
        List<Menu> x=new ArrayList<>();
        for(MenuToRestaurant i:repo.findAll()){
            if(i.getX().getRestaurant_name().equals(restaurant.getRestaurant_name())){
                x.add(i.getMenu());
            }
        }
        return x;
    }

    public List<Menu> getMenuByRestaurant2(int restaurant){
        List<Menu> x=new ArrayList<>();
        for(MenuToRestaurant i:repo.findAll()){
            if(i.getX().getRestaurant_id()==restaurant){
                x.add(i.getMenu());
            }
        }
        return x;
    }

    public List<Menu> getMenuById(int restaurantId){
        List<Menu> x=new ArrayList<>();
        for(MenuToRestaurant i:repo.findAll()){
            if(i.getX().getRestaurant_id()==restaurantId){
                x.add(i.getMenu());
            }
        }
        return x;
    }
}
