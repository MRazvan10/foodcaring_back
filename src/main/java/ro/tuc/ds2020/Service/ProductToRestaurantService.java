package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Models.ProductToMenu;
import ro.tuc.ds2020.Models.ProductToRestaurant;
import ro.tuc.ds2020.Models.Restaurant;
import ro.tuc.ds2020.Repository.ProductToRestaurantRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductToRestaurantService {
    private final ProductToRestaurantRepo repo;
    @Autowired
    public ProductToRestaurantService(ProductToRestaurantRepo repo) {
        this.repo = repo;
    }
    public ProductToRestaurant save(ProductToRestaurant x){
        return repo.save(x);
    }

    public void delete(int id){
        repo.delete(repo.findById(id).get());
    }

    public void deleteprodus(int id){
        for(ProductToRestaurant x:repo.findAll())
            if(x.getX().getProduct_id()==id){
                repo.delete(x);
            }
    }

    public List<Product> getProductByRestaurant(Restaurant restaurant){
        List<Product> x=new ArrayList<>();
        for(ProductToRestaurant i:repo.findAll()){
            if(i.getRestaurant()!=null)
            if(i.getRestaurant().getRestaurant_name().equals(restaurant.getRestaurant_name())){
                x.add(i.getX());
            }
        }
        return x;
    }
}
