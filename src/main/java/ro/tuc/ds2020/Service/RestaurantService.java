package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.Restaurant;
import ro.tuc.ds2020.Repository.RestaurantRepo;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RestaurantService {
    private final RestaurantRepo repo;

    public RestaurantService(RestaurantRepo repo) {
        this.repo = repo;
    }
    public List<Restaurant> getrestaurante(){
        return repo.findAll();
    }
    public Restaurant addrest(Restaurant r){
        return repo.save(r);
    }


    public Restaurant getbyname(String name){
        return repo.findrestbyName(name);
    }
}
