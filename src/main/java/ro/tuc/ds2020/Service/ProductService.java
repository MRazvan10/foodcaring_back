package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Repository.ProductRepo;

import java.util.List;

@Service
public class ProductService {
    private final ProductRepo repoprd;
    @Autowired
    public ProductService(ProductRepo repoprd) {
        this.repoprd = repoprd;
    }


    public Product add(Product p){
        return repoprd.save(p);
    }
    public void delete(int id){
        repoprd.delete(repoprd.findproductById(id));
    }
    public void update(int id, Product p){
        repoprd.delete(repoprd.findproductById(id));
        repoprd.save(p);
    }
    public Product findbyID(int id){
        return repoprd.findproductById(id);
    }
    public List<Product> getall(){
        return repoprd.findAll();
    }
}
