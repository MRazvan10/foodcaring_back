package ro.tuc.ds2020.Service;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.Menu;
import ro.tuc.ds2020.Repository.MenuRepo;

import java.util.List;

@Service
public class MenuService {
    private final MenuRepo repomenu;

    public MenuService(MenuRepo repomenu) {
        this.repomenu = repomenu;
    }

    public Menu add(Menu m){
        return repomenu.save(m);
    }
    public Menu findmenubyname(String name){
        for(Menu i:repomenu.findAll()){
            if (i.getMenu_name().equals(name))
                    return i;
        }
        return null;
    }
    public int id_last(){
        int max=0;
        for(Menu x:repomenu.findAll()){
            if(x.getMenu_id()>max)
                max=x.getMenu_id();
        }
        return max+1;
    }
    public Menu findmenubyID(int id){
        return repomenu.findmenuById(id);
    }

    public List<Menu> getall(){
        return repomenu.findAll();
    }
    public void delete(int id){
        repomenu.delete(repomenu.findmenuById(id));
    }
}
