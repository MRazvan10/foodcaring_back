package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.*;
import ro.tuc.ds2020.Repository.DpReporsitory;
import ro.tuc.ds2020.Repository.MenuRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class WishlistService {
    private final DpReporsitory dpReporsitory;

    @Autowired
    public WishlistService(DpReporsitory dpReporsitory) {
        this.dpReporsitory = dpReporsitory;
    }

    public DptoWishlist addWishlist(DptoWishlist p) {
         return dpReporsitory.save(p);
    }


    public List<Product> getProductByaccId(int id){
        List<Product> productList=new ArrayList<>();
        for(DptoWishlist i:dpReporsitory.findAll())
            if(i.getAccount().getAccount_id()==id)
                    productList.add(i.getProduct());
           return productList;
        }

    }


