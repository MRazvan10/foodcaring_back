package ro.tuc.ds2020.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.Models.Menu;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Models.ProductToMenu;
import ro.tuc.ds2020.Repository.ProductToMenuRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductToMenuService {
    private final ProductToMenuRepo repo;
    private final MenuService repomenu;
    @Autowired
    public ProductToMenuService(ProductToMenuRepo repo, MenuService repomenu) {
        this.repo = repo;
        this.repomenu = repomenu;
    }
    public ProductToMenu save(ProductToMenu x){
        return repo.save(x);
    }


    public void delete(int id){

            for(ProductToMenu x:repo.findAll())
                if(x.getMenu().getMenu_id()==id){
                    repo.delete(x);
                }

    }

    public void deleteprodus(int productid){
        for(ProductToMenu x:repo.findAll())
            if(x.getX().getProduct_id()==productid){
                Menu x2=x.getMenu();
                x2.setPrice(x2.getPrice()-x.getX().getProduct_price());
                repomenu.add(x2);
                repo.delete(x);
            }
    }

    public void deleteprodus(int productid,int menuid){
        for(ProductToMenu x:repo.findAll())
            if(x.getX().getProduct_id()==productid &&x.getMenu().getMenu_id()==menuid){
                Menu x2=x.getMenu();
                x2.setPrice(x2.getPrice()-x.getX().getProduct_price());
                repomenu.add(x2);
                repo.delete(x);
            }
    }



    public List<Product> getProductsByMenuid(int menuid){
        List<Product> list=new ArrayList<>();
        for(ProductToMenu i:repo.findAll()){
            System.out.println(i.getMenu().getMenu_id()+" menuid="+menuid);
            if(i.getMenu().getMenu_id()==menuid){
                list.add(i.getX());
            }
        }
        return list;
    }
}
