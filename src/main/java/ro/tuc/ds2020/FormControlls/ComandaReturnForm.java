package ro.tuc.ds2020.FormControlls;

import ro.tuc.ds2020.Models.Account;
import ro.tuc.ds2020.Models.ComandaSerialize;
import ro.tuc.ds2020.Models.Menu;
import ro.tuc.ds2020.Models.Product;

import java.util.List;

public class ComandaReturnForm {


    private List<Menu> menus;
    private List<Product> products;

    public ComandaReturnForm(){}


    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
