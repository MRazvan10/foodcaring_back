package ro.tuc.ds2020.FormControlls;


import java.util.List;

public class ComandaForm{
    private int id;
    private List<Integer> listmenuid;
    private   List<Integer> productid;
    private   String username_donator;
    private String username_persoana;
    private int accepted;
    private int primit;
    public ComandaForm(){}

    public List<Integer> getListmenuid() {
        return listmenuid;
    }

    public void setListmenuid(List<Integer> listmenuid) {
        this.listmenuid = listmenuid;
    }

    public List<Integer> getProductid() {
        return productid;
    }

    public void setProductid(List<Integer> productid) {
        this.productid = productid;
    }

    public String getUsername_donator() {
        return username_donator;
    }

    public void setUsername_donator(String username_donator) {
        this.username_donator = username_donator;
    }

    public String getUsername_persoana() {
        return username_persoana;
    }

    public void setUsername_persoana(String username_persoana) {
        this.username_persoana = username_persoana;
    }

    public int getAccepted() {
        return accepted;
    }

    public void setAccepted(int accepted) {
        this.accepted = accepted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrimit() {
        return primit;
    }

    public void setPrimit(int primit) {
        this.primit = primit;
    }
}
