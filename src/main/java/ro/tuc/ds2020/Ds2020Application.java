package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.FormControlls.ComandaForm;
import ro.tuc.ds2020.Models.ComandaSerialize;


import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
@Validated
public class Ds2020Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Ds2020Application.class);
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Ds2020Application.class, args);
        /*List<Integer> menuid=new ArrayList<>();
        menuid.add(1);
        menuid.add(2);
        menuid.add(3);

        List<Integer> productid=new ArrayList<>();
        productid.add(1);
        productid.add(2);
        productid.add(3);
        ComandaForm x=new ComandaForm();
        x.setListmenuid(menuid);
        x.setUsername_donator("iulian8");
        x.setProductid(productid);
        x.setUsername_persoana("neajutorat8");
        ComandaSerialize cmd=new ComandaSerialize();

        cmd.acceptComands("neajutorat8",1);*/




    }
}
