package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.Query;
import ro.tuc.ds2020.Models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.Product;


public interface AccountRepository extends JpaRepository<Account, Integer> {

    Account findByUsername(String username);
}
