package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.DpToAlergies;
import ro.tuc.ds2020.Models.DptoWishlist;

public interface DptoAlergiesRepo extends JpaRepository<DpToAlergies, Integer> {

}
