package ro.tuc.ds2020.Repository;

import ro.tuc.ds2020.Models.Menu;
import ro.tuc.ds2020.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MenuRepo extends JpaRepository<Menu,Integer> {
    @Query(value = "SELECT p " +
            "FROM Menu p " +
            "WHERE p.menu_id = :id"
    )
    Menu findmenuById(int id);

}
