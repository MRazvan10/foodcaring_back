package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.ProductToMenu;

public interface ProductToMenuRepo extends JpaRepository<ProductToMenu,Integer> {
}
