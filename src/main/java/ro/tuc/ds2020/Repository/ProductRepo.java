package ro.tuc.ds2020.Repository;

import ro.tuc.ds2020.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepo extends JpaRepository<Product,Integer> {
    @Query(value = "SELECT p " +
            "FROM Product p " +
            "WHERE p.product_id = :id"
    )
    Product findproductById(int id);

}
