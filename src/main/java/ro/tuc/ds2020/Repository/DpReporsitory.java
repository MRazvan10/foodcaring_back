package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.Account;
import ro.tuc.ds2020.Models.DptoWishlist;
import ro.tuc.ds2020.Models.Product;

public interface DpReporsitory extends JpaRepository<DptoWishlist, Integer> {
    Product findProductById(int id);

}

