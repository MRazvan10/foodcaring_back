package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.MenuToRestaurant;

public interface MenutoRestaurantRepo extends JpaRepository<MenuToRestaurant,Integer> {
}
