package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.Models.ProductToRestaurant;

public interface ProductToRestaurantRepo extends JpaRepository<ProductToRestaurant,Integer> {
}
