package ro.tuc.ds2020.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ro.tuc.ds2020.Models.Menu;
import ro.tuc.ds2020.Models.Restaurant;

public interface RestaurantRepo extends JpaRepository<Restaurant,Integer> {
    @Query(value = "SELECT p " +
            "FROM Restaurant p " +
            "WHERE p.restaurant_name = :name"
    )
    Restaurant findrestbyName(String name);
}
