package ro.tuc.ds2020.Controller;

import org.apache.catalina.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.Models.*;
import ro.tuc.ds2020.Service.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin()
@PreAuthorize("hasRole('MANAGER')")
@RequestMapping(value="/managerpage")
public class RestaurantManagerController {

    private final AccountService acc;
    private final ProductService productService;
    private final RestaurantService restaurantServices;
    private final MenuService menuService;
    private final ManagertoRestaurantService managertoRestaurantService;
    private final ProductToRestaurantService productToRestaurantService;
    private final ProductToMenuService productToMenuService;
    private final MenutoRestaurantService menutoRestaurantService;
    @Autowired
    public RestaurantManagerController(AccountService acc, ProductService productService,
                                       RestaurantService restaurantServices, MenuService menuService,
                                       ManagertoRestaurantService managertoRestaurantService,
                                       ProductToRestaurantService productToRestaurantService,
                                       ProductToMenuService productToMenuService,
                                       MenutoRestaurantService menutoRestaurantService) {
        this.acc = acc;
        this.productService = productService;

        this.restaurantServices = restaurantServices;
        this.menuService = menuService;
        this.managertoRestaurantService = managertoRestaurantService;
        this.productToRestaurantService = productToRestaurantService;
        this.productToMenuService = productToMenuService;
        this.menutoRestaurantService = menutoRestaurantService;
    }
    @PreAuthorize("hasAuthority('MANAGER')")
    @GetMapping(value = "/viewproducts{id}")
    public ResponseEntity<List<Product>> listproduct(@Validated @RequestParam(value = "id") int id){
       Restaurant restaurant=managertoRestaurantService.findRestbyidManager(id);
       List<Product> listReturn=productToRestaurantService.getProductByRestaurant(restaurant);
        return new ResponseEntity<>(listReturn,HttpStatus.OK);
    }
    @PreAuthorize("hasAuthority('MANAGER')")
    @GetMapping(value = "/viewmenus{id}")
    public ResponseEntity<List<Menu>> listmenus(@Validated @RequestParam(value = "id") int id){
        Restaurant restaurant=managertoRestaurantService.findRestbyidManager(id);
        List<Menu> listReturn=menutoRestaurantService.getMenuByRestaurant(restaurant);
        return new ResponseEntity<>(listReturn,HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
       @PostMapping(value = "/addproduct{id}")
    public ResponseEntity<String> addproduct(@Validated  @RequestBody Product p,@Validated @RequestParam(value = "id") int id){
        System.out.println(p.getPic()+" "+p.getProduct_description());
        Restaurant restaurant=managertoRestaurantService.findRestbyidManager(id);
        Product x=productService.add(p);
        ProductToRestaurant productToRestaurant=new ProductToRestaurant();
        productToRestaurant.setRestaurant(restaurant);
        productToRestaurant.setX(x);
        if(productToRestaurantService.save(productToRestaurant)!=null){
            return new ResponseEntity<>("\"Inserat\"",HttpStatus.OK);}
        return new ResponseEntity<>("\"Neinserat\"",HttpStatus.CONFLICT);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/addproductonmenu")
    public ResponseEntity<List<Product>> addproduct2(@Validated @RequestParam(value = "productId") int productId, @Validated @RequestParam(value = "menuId") int menuid){
        Menu x=menuService.findmenubyID(menuid);

       Product product=productService.findbyID(productId);
        x.setPrice(x.getPrice()+product.getProduct_price());
        Menu x2=menuService.add(x);
        ProductToMenu x3=new ProductToMenu();
        x3.setMenu(x2);
        x3.setX(product);
        productToMenuService.save(x3);
        return new ResponseEntity<>(productToMenuService.getProductsByMenuid(menuid),HttpStatus.OK);


    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/viewmenuproduct{menuId}")
    public ResponseEntity<List<Product>> viewmenuProduct(@Validated @RequestParam(value = "menuId") int menuid){

        return new ResponseEntity<>(productToMenuService.getProductsByMenuid(menuid),HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/addmenu{id}")
    public ResponseEntity addMenu(@Validated @RequestParam(value = "id") int id,@Validated  @RequestBody Menu p){
        Restaurant restaurant=managertoRestaurantService.findRestbyidManager(id);
        p.setPrice(Long.valueOf(0));
        Menu x=menuService.add(p);
        MenuToRestaurant x2=new MenuToRestaurant();
        x2.setMenu(x);
        x2.setX(restaurant);
        menutoRestaurantService.save(x2);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/addreducere")
    public ResponseEntity<List<Menu>> aplicareducere(@Validated @RequestParam(value = "idmenu") int idmenu,@Validated @RequestParam(value = "reducere") float reducere){
        Menu x=menuService.findmenubyID(idmenu);
        x.setPrice(x.getPrice()-(long)(x.getPrice()*reducere));
        menuService.add(x);
        return new ResponseEntity<>(menuService.getall(),HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/addrestaurant{id}")
    public ResponseEntity addrestaurant(@Validated @RequestBody Restaurant r,@Validated @RequestParam(value="id") int id){
        System.out.println(r.getAddress()+" "+r.getRestaurant_name());
        Restaurant restaurant=restaurantServices.addrest(r);
        Account manager=acc.getAccountByid(id);
        ManagertoRestaurant x=new ManagertoRestaurant();
        x.setAccount(manager);
        x.setRestaurant(restaurant);
        System.out.println(x.getRestaurant().getAddress()+x.getRestaurant().getRestaurant_id());
        System.out.println(x.getAccount().getAccount_id()+" "+x.getAccount().getUsername());

        managertoRestaurantService.save(x);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/deletemenu")
    public ResponseEntity deletemenu(@Validated @RequestParam(value="id") int id){
        menutoRestaurantService.delete(id);
        productToMenuService.delete(id);
        menuService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/deleteproductfromrestaurant")
    public ResponseEntity deleteproductfromrestaurant(@Validated @RequestParam(value="id") int id){
        productToMenuService.deleteprodus(id);
        productToRestaurantService.deleteprodus(id);
        productService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @PostMapping(value = "/deleteproductfrommenu")
    public ResponseEntity deleteproductfrommenu(@Validated @RequestParam(value="idProduct") int productid,@Validated @RequestParam(value="idMenu") int menuid){
        productToMenuService.deleteprodus(productid,menuid);
        return new ResponseEntity(HttpStatus.OK);
    }
}
