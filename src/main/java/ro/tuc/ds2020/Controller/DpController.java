package ro.tuc.ds2020.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.FormControlls.ComandaForm;
import ro.tuc.ds2020.FormControlls.ComandaReturnForm;
import ro.tuc.ds2020.Models.*;
import ro.tuc.ds2020.Service.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
//@PreAuthorize("hasRole('DISADVANTAGED')")
@RequestMapping(value = "/disadvantaged")

public class DpController {
    private final WishlistService wishlistService;
    private final ProductService productService;
    private final AccountService accountService;
    private final RestaurantService restaurantService;
    private final MenutoRestaurantService menutoRestaurantService;
    private final ProductToMenuService productToMenuService;
    private final MenuService menuService;

    public DpController(WishlistService wishlistService, ProductService productService, AccountService accountService, RestaurantService restaurantService, MenutoRestaurantService menutoRestaurantService, ProductToMenuService productToMenuService, MenuService menuService) {
        this.wishlistService = wishlistService;
        this.productService = productService;
        this.accountService = accountService;
        this.restaurantService = restaurantService;
        this.menutoRestaurantService = menutoRestaurantService;
        this.productToMenuService = productToMenuService;
        this.menuService = menuService;
    }


    // getAllproducts
    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @GetMapping(value = "/viewproducts")
    public ResponseEntity<List<Product>> listproduct() {

        return new ResponseEntity<>(productService.getall(), HttpStatus.OK);
    }

    //getRestaurants
      @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @GetMapping(value = "/viewrestaurants")
    public ResponseEntity<List<Restaurant>> listrestaurant() {
        return new ResponseEntity<>(restaurantService.getrestaurante(), HttpStatus.OK);
    }

      @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @GetMapping(value = "/viewmenusonrestaurant")
    public ResponseEntity<HashMap<Menu, List<Product>>> listmenuonrestaurant(@Validated @RequestParam(value = "restaurantId") int restaurantId) {

        List<Menu> menuList = menutoRestaurantService.getMenuById(restaurantId);
        HashMap<Menu, List<Product>> hashMaplist = new HashMap<>();
        for (Menu i : menuList) {
            List<Product> productList = productToMenuService.getProductsByMenuid(i.getMenu_id());
            hashMaplist.put(i, productList);
        }
        return new ResponseEntity<>(hashMaplist, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @GetMapping(value = "/viewwishlist")
    public ResponseEntity<List<Product>> viewWishlist(@Validated @RequestParam(value = "accUsername") String accUsername) {
        {
            Account acc = accountService.getAccountByName(accUsername);
            List<Product> productList=wishlistService.getProductByaccId(acc.getAccount_id());
            return new ResponseEntity<>(productList, HttpStatus.OK);
        }
    }


    //Add product to wishlist
    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @PostMapping(value = "/addwishlist")
    public ResponseEntity<List<Product>> addProductToWishlist(@Validated @RequestParam(value = "productId") int productId, @Validated @RequestParam(value = "accUsername") String accUsername) {
        {
            Account acc = accountService.getAccountByName(accUsername);
            Product product = productService.findbyID(productId);
            DptoWishlist dp = new DptoWishlist();
            dp.setAccount(acc);
            dp.setProduct(product);
            wishlistService.addWishlist(dp);
            return new ResponseEntity<>(wishlistService.getProductByaccId(acc.getAccount_id()), HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @PostMapping(value = "/viewcomanda")
    public ResponseEntity<ComandaReturnForm> addProductToWishlist(@RequestParam("username_persoana") String username) throws IOException, ClassNotFoundException {
            ComandaSerialize cmd=new ComandaSerialize();
            List<ComandaForm> cmdform=cmd.deserialize(username);
            List<Product> produse=new ArrayList<>();
            List<Menu> menus=new ArrayList<>();
            ComandaReturnForm returnForm=new ComandaReturnForm();
        Menu m;
        Product p;
            for(ComandaForm i:cmdform){
                for(int j:i.getListmenuid()){
                    m = menuService.findmenubyID(j);
                    menus.add(m);}
                for(int k:i.getProductid()){
                    p=productService.findbyID(k);
                    produse.add(p);
                }
            }
            returnForm.setProducts(produse);
            returnForm.setMenus(menus);
        return new ResponseEntity<>(returnForm,HttpStatus.OK);
    }
    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @PostMapping(value = "/acceptaComanda")
    public ResponseEntity acceptaComanda(@RequestParam("username_persoana") String username) throws IOException {
        ComandaSerialize cmd=new ComandaSerialize();
        cmd.acceptComands(username,0);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @PostMapping(value = "/comandaPrimita")
    public ResponseEntity primesteComanda(@RequestParam("username_persoana") String username) throws IOException {
        ComandaSerialize cmd=new ComandaSerialize();
        cmd.acceptComands(username,1);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}