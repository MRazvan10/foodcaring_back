package ro.tuc.ds2020.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.FormControlls.ComandaForm;
import ro.tuc.ds2020.Models.*;
import ro.tuc.ds2020.Service.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin()
@PreAuthorize("hasAuthority('DONNOR')")
@RequestMapping("/donnorpage")
public class DonnorController {

    private final RestaurantService restaurantService;
    private final MenutoRestaurantService menuToRestaurantService;
    private final ProductToMenuService productToMenuService;
    private final ProductService productService;
    private final MenuService menuService;
    private final AccountService accountService;
    private final WishlistService wishlistService;



    public DonnorController(RestaurantService restaurantService, MenutoRestaurantService menuToRestaurantService, ProductToMenuService productToMenuService, ProductService productService, MenuService menuService, AccountService accountService, WishlistService wishlistService) {
        this.restaurantService = restaurantService;
        this.menuToRestaurantService = menuToRestaurantService;
        this.productToMenuService = productToMenuService;
        this.productService = productService;
        this.menuService = menuService;
        this.accountService = accountService;
        this.wishlistService = wishlistService;


    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @GetMapping(value = "/viewrestaurant")
    public ResponseEntity<List<Restaurant>> getRestaurants() {
        return new ResponseEntity<>(restaurantService.getrestaurante(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @GetMapping(value = "/viewmenuonrestaurant")
    public ResponseEntity<List<Menu>> getMenuOnRestaurants(@Validated @RequestParam("idRestaurant") int idRest) {
        return new ResponseEntity<>(menuToRestaurantService.getMenuByRestaurant2(idRest), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @PostMapping(value = "/viewmenuproduct{menuId}")
    public ResponseEntity<List<Product>> viewmenuProduct(@Validated @RequestParam(value = "menuId") int menuid) {
        return new ResponseEntity<>(productToMenuService.getProductsByMenuid(menuid), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @PostMapping(value = "/viewproducts")
    public ResponseEntity<List<Product>> viewproducts() {
        return new ResponseEntity<>(productService.getall(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @PostMapping(value = "/viewmenus")
    public ResponseEntity<List<Menu>> viewmenus() {
        return new ResponseEntity<>(menuService.getall(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('DONNOR')")
    @PostMapping(value = "/persoaneneajutorateAndwish")
    public ResponseEntity<HashMap<Account, List<Product>>> viewpeopleneeds() {
        HashMap<Account, List<Product>> hashMap = new HashMap<>();
        for (Account acc : accountService.getall()) {
            List<Product> productList = wishlistService.getProductByaccId(acc.getAccount_id());
            hashMap.put(acc, productList);
        }
        return new ResponseEntity<>(hashMap, HttpStatus.OK);
    }

    //typecomanda =0 => atunci ii atribuie persoanei care e normal
    //typecomanda =1=> se atribuie persoanei catre care s-au efectuat cele mai putine comenzi.
    @PreAuthorize("hasAuthority('DONNOR')")
    @PostMapping(value = "/makeorder{typeComanda}")
    public ResponseEntity<String> makeorder(@Validated @RequestBody ComandaForm x, @Validated @RequestParam(value = "typeComanda") int type) throws IOException {
        if (type == 0) {
            x.setAccepted(0);
            x.setPrimit(0);
            ComandaSerialize cmd = new ComandaSerialize();
            cmd.serializeaza(x);

            return new ResponseEntity<>("\"Comanda Procesata\"", HttpStatus.OK);
        }
        else
            return new ResponseEntity<>("\"Comanda Procesata\"",HttpStatus.OK);
    }



}

