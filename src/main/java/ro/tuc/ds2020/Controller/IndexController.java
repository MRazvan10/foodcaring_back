package ro.tuc.ds2020.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin()
@RestController
public class IndexController {
    public IndexController() {
    }

    @GetMapping(value = "/")
    public ResponseEntity<String> getStatus() {
        return new ResponseEntity<>("City APP Service is running...", HttpStatus.OK);
    }

    @GetMapping(value = "helow")
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("City APP Service is running...", HttpStatus.OK);
    }
}


