package ro.tuc.ds2020.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.Models.Account;
import ro.tuc.ds2020.Service.AccountService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin()
@RequestMapping("/administrator")
public class AdministratorController {
    private final AccountService serv;

    @Autowired
    public AdministratorController(AccountService serv) {
        this.serv = serv;
    }

    //@PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/getaccounts")
    public ResponseEntity<List<Account>> findacc() {
        List<Account> accountList = new ArrayList<>();
        for (Account i : serv.getall())
            if (i.getRole().equals("MANAGER") || i.getRole().equals("USER"))
                accountList.add(i);
        return new ResponseEntity<>(accountList, HttpStatus.OK);
    }

    //@PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/addaccount") // add cu encrypted password
    public ResponseEntity addacc(@Valid @RequestBody Account account) {
        serv.addAccount(account.getUsername(), account.getPassword(), account.getRole());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/deleteaccount/{id}") //delete
    public void deleteRestaurant(@PathVariable("id") Integer accountId) {
        serv.delete(accountId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/update/{username}")
    public ResponseEntity<Account> updateAccount(@Valid @RequestBody Account account, @PathVariable("username") String username) {
        Account updateAccount = serv.update(username, account);
        return new ResponseEntity<>(updateAccount, HttpStatus.OK);
    }


}
