package ro.tuc.ds2020.Controller;

import net.minidev.json.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import ro.tuc.ds2020.Models.Account;
import ro.tuc.ds2020.Service.AccountService;


import javax.validation.Valid;

@RestController
@CrossOrigin()
@RequestMapping(value = "/login")
public class LoginController {

    private final AccountService accountService;

    @Autowired
    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping()
    public ResponseEntity<String> login(@RequestBody @Valid Account accountDTO) {
        String token = " { \"token\": \"" + accountService.login(accountDTO.getUsername(), accountDTO.getPassword()) + "\"} ";

        if(token != null)
            return new ResponseEntity<>(token, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }


@PostMapping("/signup")
@ResponseStatus(HttpStatus.CREATED)
public ResponseEntity<String> signup(@RequestBody @Valid Account accountDto){
    if(accountService.getAccountByName(accountDto.getUsername())!=null){
   accountService.addAccount(accountDto.getUsername(), accountDto.getPassword(),"DONNOR");
    return new ResponseEntity<>("OK",HttpStatus.OK);
    }
    else
        return new ResponseEntity<>("This account exist",HttpStatus.OK);


}



}

