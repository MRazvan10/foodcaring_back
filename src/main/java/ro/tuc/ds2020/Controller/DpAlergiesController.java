package ro.tuc.ds2020.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.Models.Account;
import ro.tuc.ds2020.Models.DpToAlergies;
import ro.tuc.ds2020.Models.DptoWishlist;
import ro.tuc.ds2020.Models.Product;
import ro.tuc.ds2020.Service.AccountService;
import ro.tuc.ds2020.Service.AlergiesService;
import ro.tuc.ds2020.Service.ProductService;

import java.util.List;
@CrossOrigin
@RestController
@PreAuthorize("hasRole('DISADVANTAGED')")
@RequestMapping(value="/disadvantaged")

public class DpAlergiesController {
    private final AlergiesService alergiesService;
    private final ProductService productService;
    private final AccountService accountService;

    public DpAlergiesController(AlergiesService alergiesService, ProductService productService, AccountService accountService) {
        this.alergiesService = alergiesService;
        this.productService = productService;
        this.accountService = accountService;
    }

    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @GetMapping(value = "/viewproducts")
    public ResponseEntity<List<Product>> listproduct() {

        return new ResponseEntity<>(productService.getall(), HttpStatus.OK);
    }
    
    //add alergies
    @PreAuthorize("hasAuthority('DISADVANTAGED')")
     @GetMapping(value = "/viewproductsalergies")
    public ResponseEntity<List<Product>> viewAlergies(@Validated @RequestParam(value = "accUsername") String accUsername) {
        {
            Account acc = accountService.getAccountByName(accUsername);
            List<Product> productList=alergiesService.getAlergiesProductByaccId(acc.getAccount_id());
            return new ResponseEntity<>(productList, HttpStatus.OK);
        }
    }
    
    //view alergies
    @PreAuthorize("hasAuthority('DISADVANTAGED')")
    @PostMapping(value = "/addalergies")
    public ResponseEntity<List<Product>> addProductToAlergies(@Validated @RequestParam(value = "productId") int productId, @Validated @RequestParam(value = "accUsername") String accUsername) {
        {
            Account acc = accountService.getAccountByName(accUsername);
            Product product = productService.findbyID(productId);
            DpToAlergies dp = new DpToAlergies();
            dp.setAccount(acc);
            dp.setProduct(product);
            alergiesService.addAlergies(dp);
            return new ResponseEntity<>(alergiesService.getAlergiesProductByaccId(acc.getAccount_id()), HttpStatus.OK);
        }
    }
}
