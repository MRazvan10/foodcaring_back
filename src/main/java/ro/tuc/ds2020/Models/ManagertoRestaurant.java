package ro.tuc.ds2020.Models;

import javax.persistence.*;
@Entity
@Table(name="ManagertoRestaurant")
public class ManagertoRestaurant {

    @Id
    @Column(name = "id")
    @GeneratedValue
    int id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "acc_id")
    Account account;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "restaurant_id")
    Restaurant restaurant;

    public ManagertoRestaurant(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
