package ro.tuc.ds2020.Models;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import ro.tuc.ds2020.FormControlls.ComandaForm;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ComandaSerialize {
    private static int nr_comenzi;

    static {
        try {
            nr_comenzi = lastline();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ComandaSerialize(){}

    public void serializeaza(ComandaForm x) throws IOException {
        JSONObject comandaJson = new JSONObject();
        comandaJson.put("id",nr_comenzi);
        comandaJson.put("listmenuid",x.getListmenuid());
        comandaJson.put("productid",x.getProductid());
        comandaJson.put("username_donator",x.getUsername_donator());
        comandaJson.put("username_persoana",x.getUsername_persoana());
        comandaJson.put("accepted",x.getAccepted());
        comandaJson.put("primit",x.getPrimit());


        try (FileWriter file = new FileWriter("comenzi.json",true)) {
            //We can write any JSONArray or JSONObject instance to the file
            file.write(comandaJson.toJSONString());
            file.write("\n");
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public List<ComandaForm> deserialize(String username_persoana) throws IOException, ClassNotFoundException {

        List<ComandaForm> retList=new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();

        File x=new File("comenzi.json");
        FileReader fr=new FileReader(x);   //reads the file
        BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
        StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters
        String line;
        while((line=br.readLine())!=null){
            ComandaForm root = objectMapper.readValue(line, ComandaForm.class);
            if(root.getUsername_persoana().equals(username_persoana))
                retList.add(root);
        }

        return retList;
    }
private static int lastline() throws IOException {
    File x=new File("comenzi.json");
    int count=0;
    String str="";

    FileReader fr=new FileReader(x);   //reads the file
    BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
    StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters
    String line;
    while((line=br.readLine())!=null){
        count++;
    }
        return count+1;
}

public void acceptComands(String username_persoana,int argument) throws IOException {

    List<ComandaForm> retList = new ArrayList<>();

    ObjectMapper objectMapper = new ObjectMapper();

    File x = new File("comenzi.json");
    FileReader fr = new FileReader(x);   //reads the file
    BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
    StringBuffer sb = new StringBuffer();    //constructs a string buffer with no characters
    String line;
    while ((line = br.readLine()) != null) {
        ComandaForm root = objectMapper.readValue(line, ComandaForm.class);
        if (root.getUsername_persoana().equals(username_persoana)){
            if(argument==0)
            root.setAccepted(1);
            else
                root.setPrimit(1);
            }
        retList.add(root);
    }
    deleteallfromFile();
    nr_comenzi=lastline();

    for(ComandaForm i:retList)
        serializeaza(i);

}


private void deleteallfromFile() throws FileNotFoundException {
    PrintWriter writer = new PrintWriter("comenzi.json");
    writer.print("");
// other operations
    writer.close();}
}
