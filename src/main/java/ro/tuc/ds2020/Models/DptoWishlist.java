package ro.tuc.ds2020.Models;


import javax.persistence.*;
@Entity
@Table(name ="DpToWishlist")
public class DptoWishlist {

    @Id
    @Column(name = "id")
    @GeneratedValue
    int id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "acc_id")
    Account account;
    @OneToOne(cascade = CascadeType.MERGE) // wishlist
    @JoinColumn(name = "product_id")
    Product product;

    public DptoWishlist() {
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public DptoWishlist(int id, Account account, Product product) {
        this.id = id;
        this.account = account;
        this.product = product;
    }
}






