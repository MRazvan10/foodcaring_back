package ro.tuc.ds2020.Models;

import javax.persistence.*;

@Entity
@Table(name = "ProductToRestaurant")
public class ProductToRestaurant {
    @Id
    @Column(name = "id")
    @GeneratedValue
    int id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "product_id")
    Product x;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "restaurant_id")
    Restaurant restaurant;

    public ProductToRestaurant(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getX() {
        return x;
    }

    public void setX(Product x) {
        this.x = x;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
