package ro.tuc.ds2020.Models;

import javax.persistence.*;
@Entity
@Table(name = "ProductToMenu")
public class ProductToMenu {
    @Id
    @Column(name = "id")
    @GeneratedValue
    int id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "product_id")
    Product x;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "menu_id")
    Menu menu;

    public ProductToMenu(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getX() {
        return x;
    }

    public void setX(Product x) {
        this.x = x;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
