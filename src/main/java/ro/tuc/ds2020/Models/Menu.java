package ro.tuc.ds2020.Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Menu")
public class Menu{
    @Id
    @Column(name = "menu_id")
    @GeneratedValue
    private int menu_id;

    @Column(name = "name", nullable = false)
    private String menu_name;

    @Column(name = "price")
    private Long price;


    public Menu() {

    }

    public Menu(int menu_id, String menu_name, Long price) {
        this.menu_id = menu_id;
        this.menu_name = menu_name;
        this.price = price;
    }

    public int getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(int menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}