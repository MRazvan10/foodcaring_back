package ro.tuc.ds2020.Models;


import javax.persistence.*;


@Entity
@Table(name="Product")
public class Product{
    @Id
    @Column(name = "product_id")
    @GeneratedValue
    private int product_id;


    @Column(name = "name", nullable = false)
    private String product_name;


    @Column(name = "picByte", length = 1000,nullable = false)
    private String pic;


    public String getName() {

        return product_name;
    }

    @Column(name = "price", nullable = false)
    private Long product_price;

    @Column(name = "description", nullable = false)
    private String product_description;


    public  Product()
    {

    }

    public Product(int product_id, String product_name, String pic, Long product_price, String product_description) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.pic = pic;
        this.product_price = product_price;
        this.product_description = product_description;

    }
    public Product( String product_name, String picByte, Long product_price, String product_description) {
        this.product_name = product_name;
        this.pic = picByte;
        this.product_price = product_price;
        this.product_description = product_description;

    }


    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Product(String product_name) {
        this.product_name = product_name;
    }


    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }



    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Long getProduct_price() {
        return product_price;
    }

    public void setProduct_price(Long product_price) {
        this.product_price = product_price;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }
}

