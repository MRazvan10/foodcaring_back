package ro.tuc.ds2020.Models;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "Restaurant")
public class Restaurant {


    private int restaurant_id;

    @Column(name = "restaurant_name", nullable = false)
    private String restaurant_name;
    @Column(name = "address", nullable = false)
    private String address;


    public Restaurant() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @Id
    @Column(name = "restaurant_id")
    @GeneratedValue
    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }




}