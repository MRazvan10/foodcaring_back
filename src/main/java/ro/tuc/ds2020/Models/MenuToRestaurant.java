package ro.tuc.ds2020.Models;

import javax.persistence.*;
@Entity
@Table(name = "MenuToRestaurant")
public class MenuToRestaurant {

    @Id
    @Column(name = "id")
    @GeneratedValue
    int id;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "restaurant_id")
    Restaurant x;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "menu_id")
    Menu menu;

    public MenuToRestaurant(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurant getX() {
        return x;
    }

    public void setX(Restaurant x) {
        this.x = x;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
